<?php
/**
 * @example Funcao para carregar todas as classes model
 *
 */

error_reporting(~E_ALL);

session_start();

$_SESSION['projeto'] = 'intranet';

$_SESSION['erro'] = isset($_SESSION['erro']) ? $_SESSION['erro'] : '';

$_SESSION['acao'] = isset($_SESSION['acao']) ? $_SESSION['acao'] : '';

$_SESSION['post'] = isset($_SESSION['post']) ? $_SESSION['post'] : '';

include_once 'autoload.php';

date_default_timezone_set('America/Sao_Paulo');

?>
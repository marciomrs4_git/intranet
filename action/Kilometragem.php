<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/controle/componentes/config.php');

if($_POST)
{
	if ($_SESSION['validacaoform'] == base64_encode(date('d-m-Y')))
	{


		$acao = base64_decode($_SESSION['acaoform']);

		switch ($acao)
		{
			case 'cadastrar/Kilometragem':

				$cadastro = new Cadastro();

				try
				{
					$cadastro->setDados($_POST);

					$cadastro->cadastrarKilometragem();

					$cadastro->finalizarApp('cadastrar/Kilometragem');

				}catch (Exception $e)
				{
					ClasseException::throwException($e,$_POST,'cadastrar/Kilometragem');
				}
				break;

			case 'alterar/Kilometragem' :
				$alteracao = new Alteracao();

				try
				{

					$alteracao->setDados($_POST);

					$alteracao->alterarKilometragem();

					$alteracao->finalizarApp();

				}catch (Exception $e)
				{
					ClasseException::throwException($e);
				}
				break;

			default:
				Sessao::destroiSessao();
				break;
					
		}

	}else
	{
		Sessao::destroiSessao();
	}
}else
{
	Sessao::destroiSessao();
}


?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/controle/componentes/config.php');

if($_POST)
{
	try
	{
		$login = new Logar();

		$login->setDados($_POST);

		$login->fazerLogin();
		
		$login->finalizarApp();
		
		
	} catch (Exception $e)
	{
		$_SESSION['erro'] = $e->getMessage();
		header('location: '.$_SERVER['HTTP_REFERER']);
	}

}
else
{
	Sessao::destroiSessao();
}

?>

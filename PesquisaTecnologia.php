<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="ISO-8859-1">
    <title>INTRANET CEADIS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="intranet-ceadis">
    <meta name="author" content="marcio@ceadis.org.br">

    <!-- Le styles -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="index.php">CEADIS -  Centro Estadual de Armazenamento e Distribui��o de insumos de Sa�de</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              <?php echo(date('d-m-Y H:i:s')); ?><a href="#" class="navbar-link"></a>
            </p>
            <ul class="nav">
            <!-- 
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
             -->
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Link de Acesso</li>
              <li class="divider"></li>
              <li><a href="http://172.22.0.42/armhazena">WMS - Sistema mHa</a></li>             
              <li><a href="http://172.22.0.33">Portal - Estoque Web</a></li>
              <li><a href="http://172.22.0.30/sga">Chamado - Gest�o de Atividade</a></li>
              <li><a href="http://172.22.0.46/">Solicita��o de Compras</a></li>              
              <li><a href="http://187.48.62.186:81/corpore.net/Login.aspx">Portal RH</a></li>
              <li><a href="http://192.168.31.250">Pesquisas ADAIA</a></li>
              <li><a href="http://192.168.31.6/adaia">ADAIA</a></li>              
              <li><a href="http://suporte.inovapar.com.br/tickets.php">Suporte Inovapar</a></li>              
              <li><a href="http://portal.mhasistemas.net/sites/projetos/CEADIS/Lists/Suporte/Por%20Situao.aspx">Suporte mHa</a></li>                                          
              <li><a href="https://sso.byyou.com/login?service=https%3A%2F%2Fsuporte.totvs.com%2Fc%2Fportal%2Flogin">Suporte TOTVS</a></li>              
              <li class="divider"></li>
              <li class="nav-header">Pesquisa</li>
              <li><a href="PesquisaTecnologia.php">Avalia��o de Servi�oes da TI</a></li>                           
           
            </ul>
            
          </div><!--/.well -->
        </div>
        
        <!--/span-->
        <div class="span9">
          <div class="hero-unit">
          
          <?php 
          
          function respostas()
          {
          	$resp = array( 0 => array('1','MUITO SATISFEITO'),
          				   1 => array('2','SATISFEITO'),
          				   2 => array('3','POUCO SATISFEITO'),
          				   3 => array('4','INSATISFEITO')
          				 );
          	
          	foreach($resp as $valor):
          		echo("<option value='{$valor[0]}'>{$valor[1]}</option>");
          	endforeach;
          	
          }
          
          ?>
            
			<form>
			  <fieldset>
			    <legend>Avalia��o de Servi�os da TI</legend>
			    <label>1) O TEMPO DO PRIMEIRO ATENDIMENTO AOS CHAMADOS?</label>
			    
			    <label>
				    <select class="span3">
						<option></option>						    
					<?php respostas(); ?>
					</select>
				</label>
				
				<ul class="nav nav-list">
  					<li class="divider"></li>
				</ul>
			    
			    <label>2) O TEMPO DE SOLUCAO DOS CHAMADOS?</label>
			    <label>
				    <select class="span3">
						<option></option>						    
						<?php respostas(); ?>
					</select>
				</label>
				
				<ul class="nav nav-list">
  					<li class="divider"></li>
				</ul>
			    
			    <label>3) O CONHECIMENTO DA EQUIPE DE TI COM RELA��O AOS SISTEMAS E EQUIPAMENTOS DO CEADIS?</label>
			    <label>
				    <select class="span3">
						<option></option>						    
						<?php respostas(); ?>
					</select>
				</label>
				
				<ul class="nav nav-list">
  					<li class="divider"></li>
				</ul>
				
			    <label>4) A COMUNICACAO COM A EQUIPE DE TI COM RELA��O AO ANDAMENTO DOS CHAMADOS?</label>
			    <label>
				    <select class="span3">
						<option></option>				    
						<?php respostas(); ?>
					</select>
				</label>
				
				
				<label>5) O SISTEMA DE ABERTURA E ACOMPANHAMENTO DE CHAMADOS?</label>
			    <label>
				    <select class="span3">
						<option></option>				    
						<?php respostas(); ?>
					</select>
				</label>
				

												
				<ul class="nav nav-list">
  					<li class="divider"></li>
				</ul>				
				
				<label>6) O QUE VOCE SUGERE DE MELHORIA PARA QUE A EQUIPE DE TI POSSA SER MAIS AGIL E RESOLVA COM QUALIDADE OS SEUS CHAMADOS?</label>
				<textarea rows="5" class="input-xxlarge"></textarea>  
				
				<hr />
				
				<label>
			    	<button type="submit" class="btn" id="finalizar">Finalizar</button>
				</label>
			  </fieldset>
			</form>
            
          </div>
      <hr>
      

		</div>
	  </div>
      <footer>
        <p>&copy; CEADIS - <?php echo(date('Y')); ?></p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
   <script src="../intranet/jscript/jquery-1.7.2.js" type="text/javascript"></script>
   
   <script type="text/javascript">

   $(document).ready(function(){

	   $("#finalizar").click(function(){
	   	alert("Obrigado por responder nossa pesquisa! Dados Enviados com sucesso.");
	   }
	   );


	   });
   
   </script>
   
  </body>
</html>

$(document).ready(function(){
	
	 $("#dialog").dialog({
      	autoOpen: false,
      	show:{
        	effect: "blind",
        	duration: 1000
      		 },
      	hide:{
	        effect: "explode",
    	    duration: 1000
      		}
    	});
 
    $("#opener").click(function(){
      $("#dialog").dialog("open");
    });
	

	$("#accordion").accordion({
		collapsible: true
	});

	$(".date").datepicker({
		dateFormat: "dd-mm-yy",
		dayNamesMin: ["Dom","Seg","Ter","Qua","Qui","Sex","S�b"],
		monthNames: ["Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
		autoSize: true,
		showAnim: "drop"
	});

	$("#alert").click(function(){
		
		alert("Ol� mundo");
		
	});

	$("#mostrarForm").hide();
	
	$("#mostrarForm").click(function(){
		$("#meuform").show('slow');
		$("#mostrarForm").hide();
	});	
	
		$('#meuform').validate({
			rules: {
				nome: { required: true, minlength: 2 },
				email: { required: true, email: true }
				
			},
			messages: {
				nome: { required: 'Preencha o campo nome', minlength: 'No m�nimo 2 letras' },
				email: { required: 'Informe o seu email', email: 'Ops, informe um email v�lido' }
				
 
			},
			submitHandler: function( form ){
				var dados = $( form ).serialize();
 
 				var icone = $('<img src="../intranet/css/images/carregar.gif">');
				var icone2 = $('<img src="../intranet/css/images/aguarde.gif">');
		
				var link = $('input[name="link"]').val();
 
				$.ajax({
					type: "POST",
					url: link,
					data: dados,
					
					beforeSend: function()
					{
						$("#conteudo").html(icone);
						$("#meusubmit").hide();
						$("#carregando").html(icone2);
					},
					complete: function()
					{
						$(icone).remove();
						$(icone2).remove();
						//$("input[name='nome']").val('');
						$("#meusubmit").show();
						$("#meuform").hide('slow');
						$("#mostrarForm").show('slow');
				
					},
					success: function( data )
					{
						$("#conteudo").load(link,function(){
							$(".table").dataTable();
						});

						//$("#conteudo").html(data).fadeIn("slow").fadeOut("slow").show("slow");
					}
				});
 
				return false;
			}
		});
	});


/*
$(document).ready(function(){
	$("#meuform").validate({
		rules:
		{
			nome:
			{
				required: true,
				minlength: 5   
			},
			email:
			{
				required: true
			}
		},

		messages:
		{
			nome:
			{
				required: "Preencha o campo <u> nome </u>"
			},
			email:
			{
				required: "Campo Email � Obrigadorio"
			}
		},
					
		submitHandler: (function(form){
			e.preventDefault();
			
			var icone = $('<img src="../intranet/css/images/carregar.gif">');
			var icone2 = $('<img src="../intranet/css/images/aguarde.gif">');
		
			var dados = $(form).serialize();
			
			var link = $("input[name='link']").val();
			
			$.ajax(
			{
				url: link,
				dataType: 'html',
				type: 'POST',
				data: dados,
			
				beforeSend: function()
				{
					$("#conteudo").html(icone);
					$("#meusubmit").hide();
					$("#carregando").html(icone2);
				},
			
				complete: function()
				{
					$(icone).remove();
					$(icone2).remove();
					$("input[name='nome']").val('');
					$("#meusubmit").show();
					$("#meuform").hide('slow');
					$("#mostrarForm").show('slow');
				
				},
			
				success: function(data,textStatus)
				{
					$("#conteudo").html('<p>'+data+'</p>');
				},
			
				error: function(xhr,er)
				{
				alert('Ocorreu Um erro!');
				}
			
			});
						
		})
	});	

	
});

*/

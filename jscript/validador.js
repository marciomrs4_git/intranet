


$(document).ready(function(){
	
	$("#meuform").validate({
		/* REGRAS DE VALIDA��O DO FORMUL�RIO */
		rules:{
			nome:{
				required: true, /* Campo obrigat�rio */
				minlength: 5    /* No m�nimo 5 caracteres */
			},
			email:{
				required: true
			}
		},
		/* DEFINI��O DAS MENSAGENS DE ERRO */
		messages:{
			nome:{
				required: "Preencha o campo <u> nome </u>"
				},
			email:{
				required: "Campo Email � Obrigadorio"
			}
		}
	});
		
	
});
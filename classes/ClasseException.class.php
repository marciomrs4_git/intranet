<?php

class ClasseException
{

	public static function throwException($e,$post = null,$form = null)
	{
			$_SESSION[$form] = $post;
			
			$_SESSION['erro'] = Texto::erro($e->getMessage());
			
			$_SESSION['acao'] = $_SESSION['acaoform'];
			$_SESSION['valor'] = $_SESSION['valorform'];

			header('location: '.$_SERVER['HTTP_REFERER']);
	}
	
}
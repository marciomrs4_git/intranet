<?php
class Busca extends Dados
{

	public function listarFinancas()
	{
		$tbFinancas = new TbCategoria();

		try 
		{

			$this->dados['data1'] = ValidarDatas::dataBanco($this->dados['data1']);
			$this->dados['data2'] = ValidarDatas::dataBanco($this->dados['data2']);
			
			$this->dados['cat_codigo'] = ($this->dados['cat_codigo'] == '') ? '%' : $this->dados['cat_codigo']; 
			
			$this->dados['ten_codigo'] = ($this->dados['ten_codigo'] == '') ? '%' :  $this->dados['ten_codigo'];

			
			$dados = $tbFinancas->listarCategoria();

			return($dados);
			
			
		} catch (Exception $e) 
		{
			throw new Exception($e->getMessage(), $e->getCode());
		}
		
	}

	public function listarTotalEntrada()
	{
		$tbFinancas = new TbFinancas();

		try
		{
			$this->dados['data1'] = ValidarDatas::dataBanco($this->dados['data1']);
			$this->dados['data2'] = ValidarDatas::dataBanco($this->dados['data2']);
				
			$dados = $tbFinancas->listarTotaisEntradaSaida($this->dados);

			return($dados);
				
		} catch (Exception $e)
		{
			throw new Exception($e->getMessage(), $e->getCode());
		}


	}

	public function listarTotalCategoria()
	{
		$tbFinancas = new TbFinancas();

		try
		{
			$this->dados['data1'] = ValidarDatas::dataBanco($this->dados['data1']);
			$this->dados['data2'] = ValidarDatas::dataBanco($this->dados['data2']);
				
			$dados = $tbFinancas->listarTotalCategoria($this->dados);

			return($dados);
				
		} catch (Exception $e)
		{
			throw new PDOException($e->getMessage(), $e->getCode());
		}

	}
	
	public function listarCalculoEntradaSaida()
	{
		$tbFinancas = new TbFinancas();

		try
		{
			$this->dados['data1'] = ValidarDatas::dataBanco($this->dados['data1']);
			$this->dados['data2'] = ValidarDatas::dataBanco($this->dados['data2']);
				
			$Entrada = $tbFinancas->getTotalEntrada($this->dados);
			$Saida = $tbFinancas->getTotalSaida($this->dados);
			
			$Total = $Entrada - $Saida;
			
			return($Total);
				
		} catch (Exception $e)
		{
			throw new PDOException($e->getMessage(), $e->getCode());
		}

	}
	
	public function getTotalDashBoard()
	{
		$tbFinancas = new TbFinancas();

		try
		{
			$this->dados['data1'] = ValidarDatas::dataBanco($this->dados['data1']);
			$this->dados['data2'] = ValidarDatas::dataBanco($this->dados['data2']);
				
			$Entrada = $tbFinancas->getTotalEntrada($this->dados);
			$Saida = $tbFinancas->getTotalSaida($this->dados);
			
			$Total = $Entrada - $Saida;
			
			return(ValidarNumeros::numeroCliente($Total));
				
		} catch (Exception $e)
		{
			throw new PDOException($e->getMessage(), $e->getCode());
		}

	}

}
?>
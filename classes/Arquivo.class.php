<?php

class Arquivo extends Dados
{

	public static function includeForm()
	{

		$arquivo = $_SERVER['DOCUMENT_ROOT'].'/'.$_SESSION['projeto'].'/forms/'.Validacao::descriptograr($_SESSION['acao']).'.php';

		if($_SESSION['acao'] != '')
		{
			self::validarFile($arquivo);
		}

		self::criarLinha();
	}

	private static function validarFile($file)
	{
		if(file_exists($file))
		{
			include_once($file);
		}else
		{
			echo 'Formul�rio ou Arquivo n�o encontrado';
		}
	}

	private static function criarLinha()
	{
		if(!empty($_SESSION['acao']))
		{
			echo('<hr />');
		}
	}

}
?>
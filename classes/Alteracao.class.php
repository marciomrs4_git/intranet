<?php
class Alteracao extends Dados
{

	public function alterarCategoria()
	{
		try
		{

			ValidarCampos::campoVazio($this->dados['cat_descricao'],'Categoria');
			ValidarCampos::campoVazio($this->dados['cat_ativo'],'Ativo');


			try
			{
					
				$this->conexao->beginTransaction();
					
				$tbCategoria = new TbCategoria();
				$tbCategoria->update($this->dados);
										
				$this->conexao->commit();
					
			}catch (PDOException $e)
			{
				$this->conexao->rollBack();
				throw new PDOException($e->getMessage(), $e->getCode());
			}
		} catch (Exception $e)
		{

			throw new Exception($e->getMessage(),$e->getCode());
		}

	}
	
	public function alterarFinancas()
	{
		try
		{


			ValidarCampos::campoVazio($this->dados['fin_valor'],'Valor');
			ValidarCampos::campoVazio($this->dados['fin_descricao'],'Descricao');
			ValidarCampos::campoVazio($this->dados['cat_codigo'],'Categoria');
			ValidarCampos::campoVazio($this->dados['ten_codigo'],'Tipo Entrada');			
			
			$this->dados['fin_valor'] = ValidarNumeros::numeroBanco($this->dados['fin_valor']);
			
			$this->dados['fin_data_cadastro'] = ($this->dados['fin_data_cadastro'] == '') ?  date("Y-m-d") : ValidarDatas::dataBanco($this->dados['fin_data_cadastro']);


			try
			{
					
				$this->conexao->beginTransaction();
					
				$tbFinancas = new TbFinancas();
				$tbFinancas->update($this->dados);
										
				$this->conexao->commit();
					
			}catch (PDOException $e)
			{
				$this->conexao->rollBack();
				throw new PDOException($e->getMessage(), $e->getCode());
			}
		} catch (Exception $e)
		{

			throw new Exception($e->getMessage(),$e->getCode());
		}

	}
	
	public function alterarKilometragem()
	{
		try
		{

			ValidarCampos::campoVazio($this->dados['ki_kilometragem'],'Kilometragem');
			ValidarCampos::campoVazio($this->dados['ki_data_inicial'],'Data Inicial');
				
			$this->dados['ki_data_atual'] = ($this->dados['ki_data_atual'] == '') ?  date("Y-m-d") : ValidarDatas::dataBanco($this->dados['ki_data_atual']);
			$this->dados['ki_data_inicial'] = ValidarDatas::dataBanco($this->dados['ki_data_inicial']);

			try
			{
					
				$this->conexao->beginTransaction();
					
				$tbKilometragem = new TbKilometragem();
				$tbKilometragem->update($this->dados);
										
				$this->conexao->commit();
					
			}catch (PDOException $e)
			{
				$this->conexao->rollBack();
				throw new PDOException($e->getMessage(), $e->getCode());
			}
		} catch (Exception $e)
		{

			throw new Exception($e->getMessage(),$e->getCode());
		}

	}
	
}
?>
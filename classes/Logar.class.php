<?php


/**
 *@author M�rcio Ramos
 *@name Classe para login
 *@version 2.0
 *@example Utilizado para controle de login
 *@return Retorna um objeto
 */
class Logar extends Dados
{

	public function fazerLogin()
	{

		try
		{

			ValidarCampos::campoVazio($this->dados['ace_usuario'],'Usu�rio');
			ValidarCampos::campoVazio($this->dados['ace_senha'],'Senha');
	
			$this->dados['ace_senha'] = Validacao::hashSenha($this->dados['ace_senha']);		
			
		}catch (CampoVazioException $e)
		{
			throw new CampoVazioException($e->getMessage(),$e->getCode());
		}
		try 
		{
			$tbacesso = new TbAcesso();
			$confirma = $tbacesso->confirmarUsuario($this->dados);
			
			if($confirma != 1)
			{
				throw new Exception('Usu�rio n�o encontrado');
				
			}elseif ($confirma == 1)
			{
				$dados = $tbacesso->getAcesso($this->dados);
								
				Sessao::criarSessao($dados);
			}
			
		} catch (PDOException $e) 
		{
			throw new PDOException($e->getMessage(),$e->getCode());
		}
		
	}
}
?>
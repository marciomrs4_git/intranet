<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="ISO-8859-1">
    <title>..:: SGA - Tecnologia da Informa��o ::..</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="intranet-ceadis">
    <meta name="author" content="marcio@ceadis.org.br">

    <!-- Le styles -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">PAINEL DE PROJETOS</a>
          <div class="nav-collapse collapse">

            <ul class="nav">
            <!-- 
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
             -->
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        
		<!-- Painel 1 -->
        <div class="span3 well">
			<div id="conteudo">
				Aprova��o
			</div>
		</div>
		
		<!-- Painel 1 -->
        <div class="span3 well">
				<!--  <div class="hero-unit">  -->
			<div id="conteudo">
				Andamento
				<div>
					Hor�rios de Corte por Dias da Semana e Centro de Custo	Ana Paula	18/02/2014	31/03/2014	Andamento
Abrir	2014020088	SGC-Sistema de Gest�o de Contratos	Thiago	06/12/2013	31/03/2014	Andamento
Abrir	2014020087	Coleta de Ponto-Fevereiro2014	Elaine	14/02/2014	18/02/2014	Andamento
Abrir	2014020086	Substitui��o das Catracas Ceadis	Jose Carlos	03/02/2014	28/02/2014	Andamento
Abrir	2014020085	Datasul-Contigencia de emiss�o de nfe	Renato	06/01/2014	28/02/2014	Andamento
Abrir	2014020084	Projeto Piloto de Separa��o por centro de Custo	Claudia	12/02/2014	21/02/2014	Andamento
				</div>
			</div>
		</div>
        
		<!-- Painel 1 -->
        <div class="span3 well">
				<!--  <div class="hero-unit">  -->
			<div id="conteudo">
				Paralisado
			</div>
		</div>        

		<!-- Painel 1 -->
        <div class="span3 well">
				<!--  <div class="hero-unit">  -->
			<div id="conteudo">
				Conclu�do
			</div>
		</div>        

		
	  </div>
      <footer>
        <p>&copy; CEADIS - <?php echo(date('Y')); ?></p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
   <script src="../intranet/jscript/jquery-1.7.2.js" type="text/javascript"></script>
  </body>
</html>

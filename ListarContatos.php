<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="ISO-8859-1">
    <title>INTRANET CEADIS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="intranet-ceadis">
    <meta name="author" content="marcio@ceadis.org.br">

    <!-- Le styles -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">CEADIS -  Centro Estadual de Armazenamento e Distribui��o de insumos de Sa�de</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              <?php echo(date('d-m-Y H:i:s')); ?><a href="#" class="navbar-link"></a>
            </p>
            <ul class="nav">
            <!-- 
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
             -->
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Link de Acesso</li>
              <li class="divider"></li>
              <li><a href="http://172.22.0.42/armhazena">WMS - Armhazena</a></li>             
              <li><a href="http://172.22.0.33">Portal - Estoque Web</a></li>
              <li><a href="http://177.184.203.235:8088/estoque">Portal - Sorocaba</a></li>              
              <li><a href="http://172.22.0.30/sga">Chamado - Gest�o de Atividade</a></li>
              <li><a href="http://172.22.0.46/">Solicita��o de Materiais</a></li>
              <li><a href="http://187.48.62.186:81/corpore.net/Login.aspx">Portal RH</a></li>
              <li><a href="http://192.168.31.250">Pesquisas ADAIA</a></li>
              <li class="divider"></li>
              
            </ul>
            
          </div><!--/.well -->
        </div>
      
        
        <!--/span-->
        <div class="span9">
          <div class="hero-unit">
            <p>Lista de Contatos.</p>
            
            
          </div>
      <hr>
      

		</div>
	  </div>
      <footer>
        <p>&copy; CEADIS - <?php echo(date('Y')); ?></p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
   <script src="../intranet/jscript/jquery-1.7.2.js" type="text/javascript"></script>
  </body>
</html>

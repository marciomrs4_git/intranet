<?php 
Sessao::validarForm('cadastrar/Kilometragem'); 
?>
<table>
	<tr>
		<td>
			<fieldset>
				<legend>Nova Kilometragem</legend>
<form name="arquivo" method="post" action="../<?php echo($_SESSION['projeto']); ?>/action/Kilometragem.php">
  <table border="0" cellspacing="5">
    <tr>
      <td colspan="2" align="center">
      	<?php Texto::mostrarMensagem($_SESSION['erro']); ?>
      </td>
    </tr>

    <tr>
      <th width="119" align="left" nowrap="nowrap">Kilometragem:</th>
      <td>
      	<input name="ki_kilometragem" type="text" value="<?php echo($_SESSION['cadastrar/Kilometragem']['ki_kilometragem']); ?>" />
      </td>
    </tr>

    <tr>
      <th width="119" align="left" nowrap="nowrap">Data Inicial:</th>
      <td>
      	<input name="ki_data_inicial" type="text" class="data" id="data-id" value="<?php echo($_SESSION['cadastrar/Kilometragem']['ki_data_inicial']); ?>" />
      </td>
    </tr>
      
    <tr>
      <th width="119" align="left" nowrap="nowrap">Data Atual:</th>
      <td>
      	<input name="ki_data_atual" type="text" class="data" id="data" value="<?php echo($_SESSION['cadastrar/Kilometragem']['ki_data_atual']); ?>" />
      </td>
    </tr>
    
    <tr>
      <td colspan="2" align="left">
	      <input type="submit" name="cadastrar" id="button" value="Salvar" />
      </td>
    </tr>
    
  </table>
</form>

</fieldset>
</td>
</tr>
</table>
<?php unset($_SESSION['cadastrar/Kilometragem']);?>
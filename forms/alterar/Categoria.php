<?php 

$tbCategoria = new TbCategoria();

$_SESSION['cadastrar/Categoria'] = $tbCategoria->getForm(base64_decode($_SESSION['valor']));


?>
<table>
	<tr>
		<td>
			<fieldset>
				<legend>Alterar Categoria</legend>
<form name="arquivo" method="post" action="../<?php echo($_SESSION['projeto']); ?>/action/Categoria.php">
  <table border="0" cellspacing="5">
    <tr>
      <td colspan="2" align="center">
      	<?php Texto::mostrarMensagem($_SESSION['erro']); ?>
      </td>
    </tr>
    
    <tr>
      <th width="119" align="left" nowrap="nowrap">Categoria:</th>
      <td>
        <input name="cat_codigo" type="hidden" value="<?php echo($_SESSION['cadastrar/Categoria']['cat_codigo']); ?>" />
      	<input name="cat_descricao" type="text" value="<?php echo($_SESSION['cadastrar/Categoria']['cat_descricao']); ?>" />
      </td>
    </tr>
    <tr>
      <th align="left" nowrap="nowrap">Ativo:</th>
	      <td>
	      	<?php 
			$tbSN = new TbSimNao();
	      	FormComponente::selectOption('cat_ativo',$tbSN->selectSimNao(),false,$_SESSION['cadastrar/Categoria']);	      	
	      	?>
	      </td>
    </tr>
    <tr>
      <td colspan="2" align="left">
	      <input type="submit" name="cadastrar" id="button" value="Salvar" />
      </td>
    </tr>
    
  </table>
</form>

</fieldset>
</td>
</tr>
</table>
<?php unset($_SESSION['cadastrar/Categoria']);?>
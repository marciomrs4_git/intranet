<?php 
Sessao::validarForm('cadastrar/Financas'); 

$tbFinancas = new TbFinancas();

$_SESSION['cadastrar/Financas'] = $tbFinancas->getForm(base64_decode($_SESSION['valor']));

?>
<table>
	<tr>
		<td>
			<fieldset>
				<legend>Alterar Financas</legend>
<form name="arquivo" method="post" action="../<?php echo($_SESSION['projeto']); ?>/action/Financas.php">
  <table border="0" cellspacing="5">
    <tr>
      <td colspan="2" align="center">
      	<?php Texto::mostrarMensagem($_SESSION['erro']); ?>
      </td>
    </tr>
   	<tr>
   	      <th width="119" align="left" nowrap="nowrap">Data:</th>
		<td>
			<input type="hidden" name="fin_codigo" value="<?php echo($_SESSION['cadastrar/Financas']['fin_codigo']); ?>">		
			<input type="text" id="data2-id" name="fin_data_cadastro" value="<?php echo(ValidarDatas::dataCliente($_SESSION['cadastrar/Financas']['fin_data_cadastro'])); ?>">
		</td>				 
    <tr>
      <th width="119" align="left" nowrap="nowrap">Valor:</th>
      <td>
      	<input name="fin_valor" type="text"  class="real" value="<?php echo(ValidarNumeros::numeroCliente($_SESSION['cadastrar/Financas']['fin_valor'])); ?>" />
      </td>
    </tr>    
    <tr>
      <th width="119" align="left" nowrap="nowrap">Descri��o:</th>
      <td>
      	<input name="fin_descricao" type="text" value="<?php echo($_SESSION['cadastrar/Financas']['fin_descricao']); ?>" />
      </td>
    </tr>
    <tr>
      <th align="left" nowrap="nowrap">Categoria:</th>
	      <td>
	      	<?php 
			$tbCategoria = new TbCategoria();
			FormComponente::$name = 'Selecione';
	      	FormComponente::selectOption('cat_codigo',$tbCategoria->listarCategoria(),true,$_SESSION['cadastrar/Financas']);	      	
	      	?>
	      </td>
    </tr>    
    <tr>
      <th align="left" nowrap="nowrap">Tipo Entrada:</th>
	      <td>
	      	<?php 
			$tbEntrada = new TbTipoEntrada();
	      	FormComponente::selectOption('ten_codigo',$tbEntrada->listarTipoEntrada(),false,$_SESSION['cadastrar/Financas']);	      	
	      	?>
	      </td>
    </tr>
    <tr>
      <td colspan="2" align="left">
	      <input type="submit" name="cadastrar" id="button" value="Salvar" />
      </td>
    </tr>
    
  </table>
</form>

</fieldset>
</td>
</tr>
</table>
<?php unset($_SESSION['cadastrar/Financas']);?>
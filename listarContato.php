<?php 
require_once '../intranet/config/config.php';

?>
<!doctype html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>INTRANET CEADIS</title>
<link rel="stylesheet" href="css/bootstrap.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel='shortcut icon' href='img/ceadisico.ico'>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="navbar-header">
	
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-principal">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	</div>
	
		<div class="container-fluid collapse navbar-collapse" id="menu-principal">
			<h4 class="navbar-text"><a href="index.php">CEADIS - Centro Estadual de Armazenamento e Distribui��o de insumos de Sa�de</a></h4>
			<h5 class="navbar-right navbar-text"><?php echo(date('d-m-Y H:i:s'));?></h5><span class="navbar-right navbar-text"></span>
		</div>
	</nav>

	
	<nav class="navbar"></nav>

	<div class="container-fluid ">
		<div class="col-sm-3">
		
		<div class="jumbotron">
			<div>	
				<img alt="CEADIS" src="img/logoceadis.png"  class="img-rounded img-thumbnail img-responsive" />
			</div>
		</div>
		
		
		
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">LINK DE ACESSO</h3>
				</div>
				<div class="panel-body">
					<ul class="list-unstyled">
					<?php 
						include_once 'links.php';
					?>					
					</ul>
				</div>
			<div class="panel-footer"></div>
		</div>

		<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">INFORMA��ES</h3>
				</div>
				<div class="panel-body">
					<ul class="list-unstyled">
				<?php 
					include_once 'linksRamais.php';
				?>
					</ul>
				</div>
			<div class="panel-footer"></div>
		</div>

	</div>
	<div class="container col-sm-9">
	
		<div class="jumbotron ">

			<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Lista de Contato Clientes</h3>
					</div>
				<div class="panel-body">
			<?php

			$Grid = new Grid();
			
			$array;
			foreach( file('./arquivos/ContatosHospitais.csv') as $dados){
				$array[] = explode(';', $dados);
			}
				
			
			$Grid->setCabecalho(array('Hospital','Respons�vel','Telefone'));
			$Grid->setDados($array);
			
			$Grid->show();
			?>
				</div>
			</div>
		</div>
		
	</div>


	</div>
	
	<footer>
		<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
			<p class="navbar-text">..:: � CEADIS [Tecnologia da Informa��o] <?php echo date('Y'); ?> ::..</p>
		</nav>
		<nav class="navbar"></nav>
	</footer>

	<script src=js/jquery.js></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.dataTables.js"></script>	
	<script src="js/my-data-table.js"></script>		

</body>
</html>
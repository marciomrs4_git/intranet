<?php 
include_once($_SERVER['DOCUMENT_ROOT']."/intranet/componentes/config.php");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>INTRANET - CEADIS</title>

	<link rel="stylesheet" type="text/css" media="screen" href="../intranet/jscript/media/css/demo_page.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../intranet/jscript/media/css/demo_table.css">
	
   <script src="../intranet/jscript/jquery-1.7.2.js" type="text/javascript"></script>   	  	   
   <script src="../intranet/jscript/jquery.dataTables.js" type="text/javascript"></script>   	   

<style type="text/css">

#container{margin: 0 auto; width: 960px; height: 500px;}

</style>


<script type="text/javascript">

$(document).ready(function(){

	$(".tabela1").dataTable({

		"oLanguage": {
			"sSearch": "Pesquisar: ",
			"sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)"
		},
		"sPaginationType": "full_numbers",
		"bPaginate": true,
		"bJQueryUI": false

		
		});
	
});


</script>

</head>
<body>
<div id="container">

<?php 

Arquivo::includeForm();

$tbCategoria = new TbCategoria();


$datagri = new DataGrid(array('ID','Descricao','Categoria','Acao'), $tbCategoria->listarCategoria());

$datagri->csstabela = 'display';

$datagri->acao = 'cadastrar/Categoria';

$datagri->mostrarDatagrid();

?>

<table class="display tabela1">
					<thead>
						<tr>
							<th></th>	
							<th></th>
							<th>DRT</th>
							<th>Nome</th>
							<th>Departamento</th>
							<th>Dia</th>
							<th>M�s</th>
						</tr>
					</thead>
					<tbody>
					<tr>
						<td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>					
						<td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>
						<td class="success">26000258</td>
						<td>FELIPE FERREIRA LOPES DOS SANTOS</td>
						<td>Unidade Dispensadora Tenente Pena</td>
						<td>8</td>
						<td>11</td>
					</tr>
					<tr>
						<td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>					
						<td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>
						<td class="success">26000258</td>
						<td>FELIPE FERREIRA LOPES DOS SANTOS</td>
						<td>Unidade Dispensadora Tenente Pena</td>
						<td>8</td>
						<td>11</td>
					</tr>
					<tr>
						<td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>					
						<td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>
						<td class="success">26000258</td>
						<td>FELIPE FERREIRA LOPES DOS SANTOS</td>
						<td>Unidade Dispensadora Tenente Pena</td>
						<td>8</td>
						<td>11</td>
					</tr>										
					<tr>
					    <td><button class="btn btn-primary btn-mini"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>
					    <td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>
						<td>26000078</td>
						<td>ROGERIO BRAGA DA SILVA</td>
						<td>Unidade Dispensadora Tenente Pena</td>
						<td>14</td>
						<td>11</td>
					</tr>
					<tr>
					    <td><button class="btn btn-primary"><span class="add-on"><i class="icon-search"></i></span> Ver</button></td>
					    <td><button class="btn btn-primary"><span class="add-on"><i class="icon-pencil"></i></span> Editar</button></td>
						<td>26000321</td>
						<td>BRUNA APARECIDA OLIVEIRA DOS SANTOS</td>
						<td>Unidade Dispensadora Tenente Pena</td>
						<td>22</td>
						<td>11</td>
					</tr>
					</tbody>
				</table>
<div>
</body>
</html>